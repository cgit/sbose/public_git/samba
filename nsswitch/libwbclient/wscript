#!/usr/bin/env python

import Options, Logs, os

# Remember to also update wbclient.h
VERSION="0.11"

# It may be useful at some point to allow Samba to build against a
# system libwbclient, such as the one provided by Likewise.  To to
# this, not only must the check below be activated but this must only
# be activated with an off-by-default option to disable the internal
# build of both winbindd implementations, and all the internal
# references to libwbclient.h will need to be fixed to point at the
# system libwbclient.  Finally, as a system libwbclient would probably
# not use the same version scheme as Samba, so this would need to
# reference Likewise version numbers instead.
#
#def configure(conf):
#    if conf.CHECK_BUNDLED_SYSTEM_PKG('wbclient', minversion=VERSION):
#        conf.define('USING_SYSTEM_LIBWBCLIENT', 1)
#

def build(bld):
#    if bld.CONFIG_SET('USING_SYSTEM_LIBWBCLIENT'):
#        Logs.info("\tSelected system libwbclient build")
#        return
#
#    Logs.info("\tSelected embedded libwbclient build")

    abi_match = 'wbc*'
    bld.SAMBA_LIBRARY('wbclient',
                      source='''
                             wbc_guid.c
                             wbc_idmap.c
                             wbc_idmap_common.c
                             wbclient.c
                             wbclient_common.c
                             wbc_pam.c
                             wbc_pwd.c
                             wbc_sid.c
                             wbc_sid_common.c
                             wbc_util.c
                             wbc_util_common.c''',
                      deps='winbind-client',
                      pc_files='wbclient.pc',
                      public_headers='wbclient.h',
                      abi_directory='ABI',
                      abi_match=abi_match,
                      vnum=VERSION)

    if bld.CONFIG_SET('WITH_WBCLIENT_SSSD'):
        version = "wbclient_%s" % VERSION
        bld.ABI_VSCRIPT('wbclient', 'ABI', version,
                        'wbclient-sssd.vscript', abi_match)
        vscript = os.path.join(bld.path.abspath(bld.env), 'wbclient-sssd.vscript')

        bld.SAMBA_LIBRARY('wbclient-sssd',
                          source='''
                                 wbc_guid.c
                                 wbc_idmap_sssd.c
                                 wbc_idmap_common.c
                                 wbclient_sssd.c
                                 wbclient_common.c
                                 wbc_pam_sssd.c
                                 wbc_pwd_sssd.c
                                 wbc_sid_sssd.c
                                 wbc_sid_common.c
                                 wbc_util_sssd.c
                                 wbc_util_common.c''',
                          deps='replace sss_nss_idmap',
                          pc_files='wbclient-sssd.pc',
                          public_headers='wbclient-sssd.h',
                          abi_directory='ABI',
                          abi_match=abi_match,
                          vscript = vscript,
                          vnum=VERSION)
